using UnrealBuildTool;

public class StickProjectTarget : TargetRules
{
	public StickProjectTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("StickProject");
	}
}
